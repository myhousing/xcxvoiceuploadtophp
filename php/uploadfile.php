<?php
/**
 * Created by http://www.h5tpl.com.
 * User: LiBiao 
 * Date: 2018/1/28
 * Time: 下午3:10
 */

// 封装返回json数据格式方法
function json_response($c=0, $m='', $d='') {
    $data = array(
        'code'     =>  $c,
        'msg'     =>  $m,
        'data'     =>  $d
    );
    return json_encode($data);
}


// 接收小程序传递参数（即：小程序录音市场参数等）
$params = json_decode(file_get_contents("php://input"), true);
$audio_ms = $params['audio_ms'];

// 接收小程序上传录音文件
if(!array_key_exists('file', $_FILES)) {
    return json_response(-1,'未传入文件',$readPath);
}

// 获取临时文件路径并构造新文件名称
$file = $_FILES['file']['tmp_name'];
$randFileName = 'weapp_audio_' . md5(uniqid("", true));

// 将临时文件保存到服务器中
$savePath = '/data/wwwroot/www.h5tpl.com/web/uploads/xcxvoice/' . $randFileName . '.silk';
file_put_contents($savePath, file_get_contents($file));

//获取服务器中保存的录音文件路径并返回到小程序客户端中
$readPath = 'https://www.h5tpl.com/uploads/xcxvoice/' . $randFileName . '.silk';
return json_response(200,'上传成功',$readPath);